BEGIN {
	FS=": "
}

($1 == "Package" || $1 == "Source") && $2 == name {
	matched = 1
}

$1 == field {
	value = $2
}

/^$/ {
	if (matched) { print value }
	matched = 0
}
