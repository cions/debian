# debian

My local apt repository

## Setup

```sh
$ DIST="$(. /etc/os-release; echo "${VERSION_CODENAME}")"
$ curl -fsSL https://cions.gitlab.io/debian/gpgkey | apt-key add -
$ curl -fsSL https://cions.gitlab.io/debian/"${DIST}".list >> /etc/apt/sources.list
```
