#!/bin/bash

set -euo pipefail

export DEBFULLNAME="${GITLAB_USER_NAME}"
export DEBEMAIL="${GITLAB_USER_EMAIL}"
export DEBIAN_FRONTEND="noninteractive"
export QUILT_PATCHES="debian/patches"

DIST="$(. /etc/os-release; echo "${VERSION_CODENAME}")"
PACKAGES="${CI_PROJECT_DIR}/Packages"

begin() {
	if [[ "${GITLAB_CI-}" == "true" ]]; then
		echo -e "section_start:$(date +%s):$1\r\e[0K\e[1;33m$2\e[m"
	else
		echo -e "\e[1;33m$2\e[m"
	fi
}

end() {
	if [[ "${GITLAB_CI-}" == "true" ]]; then
		echo -e "section_end:$(date +%s):$1\r\e[0K"
	fi
}

info() {
	echo -e "\e[36m$1\e[m"
}

error() {
	echo -e "\e[1;31m$1\e[m" >&2
}

exists() {
	[[ -e "$1" ]]
}

fileshash() {
	tar --sort=name --mtime='@0' -cf - "$@" | b2sum -l 32 | head -c 8
}

extract() {
	local PKGNAME="$1"
	local FIELD="$2"
	awk -f "${CI_PROJECT_DIR}/extract.awk" -v name="${PKGNAME}" -v field="${FIELD}" "${PACKAGES}"
}

download_debs() {
	local PKGNAME="$1"
	local FILENAME
	extract "${PKGNAME}" "Filename" | while IFS= read -r FILENAME; do
		info "Downloading ${FILENAME##*/}"
		curl -fsSLO "${CI_PAGES_URL}/${FILENAME}"
	done
}

builddeb() {
	if [[ -x debsetup.sh ]]; then
		builddeb_debsetup "${PKGNAME}"
	elif exists ./*.patch; then
		builddeb_patches "${PKGNAME}"
	else
		error "error: ${PKGNAME}: missing debsetup.sh or *.patch"
		return 1
	fi
}

builddeb_debsetup() {
	local PKGNAME="$1"
	local PKGVER REPOVER DEBIANFILES

	PKGVER="$(dpkg-parsechangelog -l changelog -S Version)" || return 1

	if [[ "${NO_CACHE:-0}" != "1" && -s "${PACKAGES}" ]]; then
		REPOVER="$(extract "${PKGNAME}" "Version" | head -1)" || return 1
	fi
	if [[ -n "${REPOVER:+set}" ]]; then
		info "in the repository: ${PKGNAME}_${REPOVER}"
		info "to be built: ${PKGNAME}_${PKGVER}"
		if dpkg --compare-versions "${REPOVER}" ge "${PKGVER}"; then
			info "${PKGVER} already exists in the repository. Skip to build"
			download_debs "${PKGNAME}"
			return 0
		fi
	fi

	GLOBIGNORE=".*:debsetup.sh"
	DEBIANFILES=( * )
	unset GLOBIGNORE
	mkdir debian || return 1
	mv -- "${DEBIANFILES[@]}" debian/ || return 1
	./debsetup.sh "${PKGVER}" || return 1
	exists ./*.deb && return 0
	mv debian/ "${PKGNAME}"-*/ || return 1

	cd "${PKGNAME}"-*/ >/dev/null || return 1
	mk-build-deps -ir --tool='apt-get -qy --no-install-recommends -o Debug::pkgProblemResolver=yes' || return 1
	debchange --release -D "${DIST}" "" || return 1
	dpkg-buildpackage -us -uc -b || return 1
}

builddeb_patches() {
	local PKGNAME="$1"
	local PKGVER REPOVER PATCHHASH PATCHREV
	export DEB_BUILD_OPTIONS=nocheck

	PATCHHASH="$(fileshash ./*.patch)" || return 1
	PATCHREV=1
	if [[ "${NO_CACHE:-0}" != "1" && -s "${PACKAGES}" ]]; then
		REPOVER="$(apt-cache policy "${PKGNAME}" | awk '$1=="Candidate:" { print $2 }')"
		info "in the repository: ${PKGNAME}_${REPOVER}"
		info "patchhash: ${PATCHHASH}"
		if [[ "${REPOVER}" == *+patched* && "${REPOVER##*~}" == "${PATCHHASH}" ]]; then
			info "${REPOVER} already exists in the repository. Skip to build"
			download_debs "${PKGNAME}"
			return 0
		fi
		if [[ "${REPOVER}" == *+patched* ]]; then
			PATCHREV="${REPOVER##*+patched}"
			PATCHREV=$(( ${PATCHREV%~*} + 1 ))
		fi
	fi

	apt-get build-dep -qy "${PKGNAME}" || return 1
	apt-get source "${PKGNAME}" || return 1
	cd "${PKGNAME}"-*/ >/dev/null || return 1
	quilt import ../*.patch || return 1
	PKGVER="$(dpkg-parsechangelog -S Version)+patched${PATCHREV}~${PATCHHASH}" || return 1
	debchange -D "${DIST}" -v "${PKGVER}" "User patches applied" || return 1
	dpkg-buildpackage -us -uc -b || return 1
}


if [[ "$(dpkg-divert --truename /usr/bin/man)" == "/usr/bin/man.REAL" ]]; then
	rm -f /usr/bin/man
	dpkg-divert --quiet --remove --rename /usr/bin/man
fi

begin "APTGET" "Installing packages"
sed -i '/^deb /{p;s/^deb/deb-src/}' /etc/apt/sources.list
apt-get update -q
apt-get install -qy --no-install-recommends \
	ca-certificates \
	curl \
	devscripts \
	equivs \
	git \
	gnupg \
	jq \
	quilt \
	unzip
end "APTGET"

begin "SETUPGOLANG" "Installing golang"
GOVERSION="$(curl -fsSL "https://golang.org/dl/?mode=json" | jq -r '.[0].version')"
echo "Go Version: ${GOVERSION}"
curl -fsSL "https://golang.org/dl/${GOVERSION}.linux-amd64.tar.gz" | tar zxf - -C /usr/local
export PATH="/usr/local/go/bin:${PATH}"
end "SETUPGOLANG"

begin "SETUPRUST" "Installing rust"
curl -fsSL https://sh.rustup.rs | sh -s -- -y --quiet --no-modify-path --profile minimal
export PATH="${HOME}/.cargo/bin:${PATH}"
end "SETUPRUST"

begin "ADDREPO" "Add repository"
if [[ "${NO_CACHE:-0}" != "1" ]]; then
	curl -fsSLO "${CI_PAGES_URL}/dists/${DIST}/main/binary-amd64/Packages" || true
fi
if [[ -s "${PACKAGES}" ]]; then
	curl -fsSL "${CI_PAGES_URL}/gpgkey" | APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=1 apt-key add -
	curl -fsSL "${CI_PAGES_URL}/${DIST}.list" >> /etc/apt/sources.list
	apt-get update -q
fi
end "ADDREPO"

mkdir -p "pool/${DIST}"
echo "$(. /etc/os-release; echo "${VERSION_ID}")" > "pool/${DIST}/VERSION_ID"

PKGNAMES=( packages/*/ )
PKGNAMES=( "${PKGNAMES[@]%/}" )
PKGNAMES=( "${PKGNAMES[@]##*/}" )

FAILEDPKGS=()

for PKGNAME in "${PKGNAMES[@]}"; do
	begin "BUILD-${PKGNAME}" "Building ${PKGNAME}"
	pushd "packages/${PKGNAME}" >/dev/null
	if ! ( builddeb "${PKGNAME}" ); then
		FAILEDPKGS+=( "${PKGNAME}" )
		error "error: ${PKGNAME}: failed to build"
		rm -f "${PKGNAME}-build-deps_*.deb"
		if [[ -s "${PACKAGES}" ]] && ! exists ./*.deb; then
			download_debs "${PKGNAME}" || true
		fi
	fi
	exists ./*.deb && mv ./*.deb "${CI_PROJECT_DIR}/pool/${DIST}"
	popd >/dev/null
	end "BUILD-${PKGNAME}"
done

if [[ "${#FAILEDPKGS[@]}" -gt 0 ]]; then
	error "Failed to build following packages:"
	printf '  * %s\n' "${FAILEDPKGS[@]}" >&2
	exit 1
fi
