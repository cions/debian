#!/bin/bash

set -euo pipefail

genrelease() {
	local DIST="$1"
	local VERSION_ID
	read -r VERSION_ID < "pool/${DIST}/VERSION_ID"
	rm -f "pool/${DIST}/VERSION_ID"

	echo "Suite: ${DIST}"
	echo "Codename: ${DIST}"
	echo "Version: ${VERSION_ID}"
	echo "Architectures: amd64"
	echo "Components: main"
	echo "Description: ${GITLAB_USER_NAME}'s debian repository"
	apt-ftparchive release "dists/${DIST}"
}

export DEBIAN_FRONTEND="noninteractive"
GPG="gpg --batch --pinentry-mode loopback --passphrase ${GPG_PASSPHRASE}"

apt-get update -qq
apt-get install -qy --no-install-recommends apt-utils gnupg

${GPG} --import "${GPG_KEY}"

DISTS=( pool/* )

mkdir -p public/pool
rm -rf "${DISTS[@]/#/public/}"
mv "${DISTS[@]}" public/pool

DISTS=( "${DISTS[@]#pool/}" )

pushd public >/dev/null
gpg --export --armor > gpgkey
for DIST in "${DISTS[@]}"; do
	echo "deb ${CI_PAGES_URL} ${DIST} main" > "${DIST}.list"
	mkdir -p "dists/${DIST}/main/binary-amd64"
	apt-ftparchive packages "pool/${DIST}" > "dists/${DIST}/main/binary-amd64/Packages"
	gzip -9 --force --keep "dists/${DIST}/main/binary-amd64/Packages"
	apt-ftparchive contents "pool/${DIST}" | gzip -9 > "dists/${DIST}/Contents-amd64.gz"
	genrelease "${DIST}" > "Release-${DIST}"
	mv "Release-${DIST}" "dists/${DIST}/Release"
	rm -f "dists/${DIST}/Release.gpg" && ${GPG} --armor --detach-sign -o "dists/${DIST}/Release.gpg" "dists/${DIST}/Release"
	rm -f "dists/${DIST}/InRelease" && ${GPG} --armor --clear-sign -o "dists/${DIST}/InRelease" "dists/${DIST}/Release"
done
popd >/dev/null
