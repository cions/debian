#!/bin/sh

set -eu

PKGNAME=${PWD##*/}
PKGVER="$1"
DESTDIR="${PWD}/${PKGNAME}-${PKGVER%%-*}"
TMPDIR="${PWD}/tmp"

export CARGO_HOME="${TMPDIR}/.cargo"

cargo install --version "${PKGVER%%-*}" --bins --all-features --root "${DESTDIR}" "${PKGNAME}"

rm -rf "${TMPDIR}"
