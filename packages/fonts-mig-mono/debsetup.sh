#!/bin/sh

set -eu

PKGNAME="${PWD##*/}"
PKGVER="$1"

curl -fsSL "https://github.com/cions/mig-mono/releases/download/v${PKGVER%%-*}/mig-mono-${PKGVER%%-*}.tar.xz" | tar Jxf -
mv "mig-mono-${PKGVER%%-*}" "${PKGNAME}-${PKGVER%%-*}"
