#!/bin/sh

set -eu

PKGNAME=${PWD##*/}
PKGVER="$1"
DESTDIR="${PWD}/${PKGNAME}-${PKGVER%%-*}"
TMPDIR="${PWD}/tmp"

export GOBIN="${DESTDIR}"
export GOCACHE="${TMPDIR}/gocache"
export GOMODCACHE="${TMPDIR}/gomodcache"
export GOPATH="${TMPDIR}/gocache"

go install -trimpath -modcacherw -ldflags "-s -w" "github.com/cions/goenc@v${PKGVER%%-*}"

rm -rf "${TMPDIR}"
