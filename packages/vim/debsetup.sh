#!/bin/sh

set -eu

PKGNAME="${PWD##*/}"
PKGVER="$1"
PKGVER="${PKGVER#*:}"
DESTDIR="${PWD}/${PKGNAME}-${PKGVER%%-*}"
TMPDIR="${PWD}/tmp"

curl -fsSL "https://github.com/vim/vim/archive/v${PKGVER%%-*}.tar.gz" | tar zxf -
