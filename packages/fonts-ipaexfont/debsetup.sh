#!/bin/sh

set -eu

PKGNAME="${PWD##*/}"
PKGVER="$1"

curl -fsSLO "https://moji.or.jp/wp-content/ipafont/IPAexfont/IPAexfont${PKGVER%%-*}.zip"
unzip "IPAexfont${PKGVER%%-*}.zip"
mv "IPAexfont${PKGVER%%-*}" "${PKGNAME}-${PKGVER%%-*}"
