#!/bin/sh

set -eu

PKGNAME="${PWD##*/}"
PKGVER="$1"

apt-get install -q --no-install-recommends p7zip

curl -fsSLO "http://osdn.dl.osdn.jp/users/8/8597/mgenplus-${PKGVER%%-*}.7z"
7zr x -o"${PKGNAME}-${PKGVER%%-*}" "mgenplus-${PKGVER%%-*}.7z"
