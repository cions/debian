#!/bin/sh

set -eu

PKGNAME="${PWD##*/}"
PKGVER="$1"
PKGVER="${PKGVER%%-*}"

curl -fsSLO "http://osdn.dl.osdn.jp/mix-mplus-ipa/72510/migmix-1m-${PKGVER}.zip"
curl -fsSLO "http://osdn.dl.osdn.jp/mix-mplus-ipa/72510/migmix-1p-${PKGVER}.zip"
curl -fsSLO "http://osdn.dl.osdn.jp/mix-mplus-ipa/72510/migmix-2m-${PKGVER}.zip"
curl -fsSLO "http://osdn.dl.osdn.jp/mix-mplus-ipa/72510/migmix-2p-${PKGVER}.zip"
curl -fsSLO "http://osdn.dl.osdn.jp/mix-mplus-ipa/72511/migu-1c-${PKGVER}.zip"
curl -fsSLO "http://osdn.dl.osdn.jp/mix-mplus-ipa/72511/migu-1m-${PKGVER}.zip"
curl -fsSLO "http://osdn.dl.osdn.jp/mix-mplus-ipa/72511/migu-1p-${PKGVER}.zip"
curl -fsSLO "http://osdn.dl.osdn.jp/mix-mplus-ipa/72511/migu-2m-${PKGVER}.zip"

for f in *.zip; do
	unzip "${f}"
done

mkdir "${PKGNAME}-${PKGVER}"
mv -- */*.ttf "${PKGNAME}-${PKGVER}"
mv -n -- */*.txt "${PKGNAME}-${PKGVER}"
