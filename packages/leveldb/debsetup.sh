#!/bin/sh

set -eu

PKGNAME=${PWD##*/}
PKGVER="$1"
DESTDIR="${PWD}/${PKGNAME}-${PKGVER%%-*}"
TMPDIR="${PWD}/tmp"

export GOBIN="${DESTDIR}"
export GOCACHE="${TMPDIR}/gocache"
export GOMODCACHE="${TMPDIR}/gomodcache"
export GOPATH="${TMPDIR}/gocache"

go get -trimpath -modcacherw -ldflags "-s -w" "github.com/cions/leveldb-cli/cmd/leveldb@v${PKGVER%%-*}"

rm -rf "${TMPDIR}"
