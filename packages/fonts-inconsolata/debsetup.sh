#!/bin/sh

set -eu

PKGNAME="${PWD##*/}"
PKGVER="$1"

curl -fsSLO "https://github.com/googlefonts/Inconsolata/releases/download/v${PKGVER%%-*}/fonts_ttf.zip"
unzip fonts_ttf.zip
mv fonts "${PKGNAME}-${PKGVER%%-*}"
