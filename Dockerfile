ARG IMAGE="ubuntu:latest"

FROM alpine:latest AS gnupg

RUN apk add --no-cache gnupg \
 && gpg --batch --pinentry-mode loopback --passphrase "password" \
        --quick-gen-key "gitlab-ci" ed25519 \
 && gpg --batch --pinentry-mode loopback --passphrase "password" \
        --export-secret-keys --armor --output /signkey.gpg

FROM ${IMAGE}

WORKDIR /builds

ENV DEBIAN_FRONTEND="noninteractive"
RUN apt-get update -qq \
 && apt-get install -qy --no-install-recommends \
        apt-utils \
        build-essential \
        ca-certificates \
        curl \
        debhelper \
        devscripts \
        equivs \
        git \
        gnupg \
        quilt \
        unzip \
 && rm -rf /var/lib/apt/lists/*

COPY --from=gnupg /signkey.gpg .

ENV CI_PAGES_URL="http://nginx"
ENV CI_PROJECT_DIR="/builds"
ENV GITLAB_USER_EMAIL="gitlab-ci@users.noreply.gitlab.com"
ENV GITLAB_USER_NAME="gitlab-ci"
ENV GPG_KEY="/builds/signkey.gpg"
ENV GPG_PASSPHRASE="password"

COPY . .

CMD ["/bin/sh", "-c", "./build.sh; status=$?; ./deploy.sh && exit ${status}"]
